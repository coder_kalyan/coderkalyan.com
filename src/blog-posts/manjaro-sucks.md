---
slug: "/blog/manjaro-sucks"
date: "2020-07-02"
title: "Manjaro Sucks!"
draft: false
---

## Intro
I recently installed Linux on a couple of my friends' laptops. As they
are not very knowledgeable about these things and just wanted something that
worked out of the box, I decided to go with Manjaro. Specifically, I decided to
try out Manjaro GNOME edition, since I've found KDE to be a bit cluttered for my
taste. The experience is overall pretty decent, but there are a few things that
could definitely be improved. This has nothing to do with app support, etc; it
is soley about user experience when using the OS and desktop itself.

## Disclaimer
This is my *very harsh* critique of Manjaro. Please don't get me
wrong; I am a huge proponent of open source software and I love Linux and the
GNU/Linux ecosystem. I think many distributions including Manjaro are doing a
great job of making Linux more accessible to everyday users.  The rest of this
article might sound disproportionately harsh; I am merely trying to set a very
high bar for us in the Linux developer community to look up to.

## 0. The Website
The Manjaro website is pretty decent, to be honest. However,
it could be made a little simpler to understand for a newcomer. Imagine that you
are completely new to Linux. You understand that an operating system is the
lowest level of software that makes your computer run. However, you don't know
anything about how Linux works, what a desktop environment is, how to flash an
ISO (or even what an ISO is to begin with), or how to get into BIOS and disable
secure boot and fast boot, boot into the USB, etc. You have discovered that
Manjaro is a free operating system that is fully open source and respects users'
privacy.

Now, say you were to visit [manjaro's website](https://manjaro.org). You're
interested in installing it, but you don't know where to start. You make your
way to the download section, and are presented with a few options. I think that
this is unecessary; I understand that more advanced users would want a choice -
but this just adds unecessary confusion for newcomers. I think a better approach
would be to have a flagship desktop environment (probably GNOME or KDE) and dump
the other environments on another page.

## 1. The ISO
Ok, so you've downloaded an ISO. However, you don't really know what exactly to
do with it. Now, of course there are thousands of tutorials online for how to
flash an ISO, but I think Manjaro should provide its own for a few reasons:

1. It is an unecessary inconvenience to go to another website to find a tutorial
2. Some of these sites are more trustworthy than others, and I think it would be
   more comforting to have "proven" instructions that work.
3. Many tutorials don't work properly when run on different operating systems,
   which is why there is a need for standardization.
4. There are so many tools to choose from, it can be daunting! Manjaro should
   recommend a tool.
5. It's not just about how to flash the ISO, but how to boot it as well. This is
   especially lacking in current distributions/tutorials.

## 2. The Installer
There are a few things wrong with the installer:

### Secure Boot
I have yet to come across a Linux distribution that (reliably) supports secure
boot out of the box. This means not only the installed OS, but the installer as
well should be secure boot compatible. The majority of people installing Linux
as a secondary OS are doing so on a laptop that is probably running an OEM
installation of Windows, most likely with secure boot enabled. Now I understand
that this isn't a very trivial task - but I think its possible. For instance,
there are a couple of bootloaders available that have been signed with
Microsoft's key, including *shim*. This is far from ideal, but it's better than
nothing: the primary goal is to get the OS to boot without any BIOS/UEFI
configuration.

### No Bootsplash
There's no bootsplash! It's pretty trivial to get a bootsplash going; you can
use Plymouth, or if you're in the mood, even write your own simple boot splash
system with some DRM/KMS code.

### Wifi Drivers
Specifically, Realtek. Come on, please. This has nothing to do with Manjaro in
particular; it plagues all distros. The solution is simple: INCLUDE MORE REALTEK
WIRELESS CARD DRIVERS OUT OF THE BOX. Most of them are cleanly licensed, so I
don't see the problem with adding them out of the box. They are relatively easy
to install, and save the user *hours of work* in the (unfortunately not
uncommon) scenario that the kernel's built in drivers don't work. Please. You
can't assume that the user will have an ethernet connection handy. Nor can you
assume that they are able to use Android's USB tethering feature (although this
is quite handy).

### Calamares Drive Detection
Calamares is a great installer. That said, I've had trouble with its drive
selection tool on multiple occasions. For instance, (I think) it filters out
drives that are too small to install Manjaro on, but I might be mistaken on
that. If it does, a warning such as "This drive doesn't have enough space to
install Manjaro!" would be helpful.  Also, I was trying to install onto a USB
stick the other day, and calamares didn't detect the USB stick until I opened it
with `cfdisk` and deleted all the existing partitions. This is *unacceptable*
for an otherwise fantastic install system.

## First Impressions

Once installed, I proceeded to reboot into Manjaro to check it out.

### Still no Bootsplash
Come on, Manjaro! The installer is one thing, but nobody wants to look at
systemd's boot logs. Add a bootsplash already!

Actually, Manjaro sometimes does this thing where it disables all logging,
resulting in a black LCD screen while the OS is booting, which makes you think
you broke your system. Not cool.

### Time is Off
Yeah, I know. Windows does some hocus pocus with UTC vs localtime being stored
on the motherboard's hardware clock. But come on! I booted up and the system
time was incorrect. This is downright unacceptable. Dual booting with Windows is
a common enough use case; fix it already. If you can't fix it, at least *provide
the user with clear, detailed documentation on how to go into Windows and fix
the problem* (I think I read somewhere that it can be fixed with some registry
editing on Windows).

Other than that, everything is pretty nice. Great job, Manjaro team!

## Pamac
I should probably write another article on this, but I'll mention it briefly
here. TLDR: Pamac sucks, and OctoPi is worse.

The idea of a graphical package installation tool is *NOT* to provide buttons
for accessing the package manager's functions. Someone who wants to use the
package manager as a package manager will probably find it more convenient to
fire up a terminal and use the CLI interface.

The point, rather, is to provide a *software center* that allows the user to
quickly and seamlessly install any application they might desire. Note that
there are some clear differences:
* The *Install* button should immediately install the package, not add it to a
    list that has to be applied by clicking the *Apply* button.
* There is *no need* to list the literal name of the package (the one that's
    lower case and hyphen separated). Freedesktop AppStream exists, and is a
    great way of collecting nicely formatted metadata about an application to
    display (such as title, description, and icon).
* Packages that provide tools or libraries rather than user facing applications
    should be separated and put in a secondary list to minimize confusion.
* The AUR is not optional. It is a fundamental building block of Arch-based
    distributions, and is one of the reasons Arch is so powerful. Making the AUR
    opt-in via a hidden preference menu, and listing it separately, is not very
    useful. Just tag applications as AUR in the list so the user knows that they
    are, but don't make a big deal out of it.
* Search needs to be better. It's not bad, but better search results in a nicer
    and faster experience for the user.

## Conclusion
That's it for now. These are just some of the first impressions I had with
Manjaro GNOME. Again, please don't take this the wrong way. Manjaro is a great
distribution and still one of my best recommendations for people who are new to
Linux. (I don't recommend Ubuntu anymore for various reasons, I'll write another
article on this). But the little things like this are the last 10% of
technicalities left to be addressed in Linux on desktop. We're almost there,
guys! Let's not forget about the details. It's these details that make the
difference between the desktop Linux being *pretty decent* and *exceptional*!
