---
slug: "/blog/manjaro-sucks" 
date: "2020-07-02"
title: "Manjaro Sucks!"
draft: true
---

## Intro
I recently installed Linux on a couple of my friends' laptops. As they
are not very knowledgeable about these things and just wanted something that
worked out of the box, I decided to go with Manjaro. Specifically, I decided to
try out Manjaro GNOME edition, since I've found KDE to be a bit cluttered for my
taste. The experience is overall pretty decent, but there are a few things that
could definitely be improved. This has nothing to do with app support, etc; it
is soley about user experience when using the OS and desktop itself.

## Disclaimer
This is my *very harsh* critique of Manjaro. Please don't get me
wrong; I am a huge proponent of open source software and I love Linux and the
GNU/Linux ecosystem. I think many distributions including Manjaro are doing a
great job of making Linux more accessible to everyday users.  The rest of this
article might sound disproportionately harsh; I am merely trying to set a very
high bar for us in the Linux developer community to look up to.

## 0. The Website
The Manjaro website is pretty decent, to be honest. However,
it could be made a little simpler to understand for a newcomer. Imagine that you
are completely new to Linux. You understand that an operating system is the
lowest level of software that makes your computer run. However, you don't know
anything about how Linux works, what a desktop environment is, how to flash an
ISO (or even what an ISO is to begin with), or how to get into BIOS and disable
secure boot and fast boot, boot into the USB, etc. You have discovered that
Manjaro is a free operating system that is fully open source and respects users'
privacy.

Now, say you were to visit [manjaro's website](https://manjaro.org). You're
interested in installing it, but you don't know where to start. You make your
way to the download section, and are presented with a few options. This is where
you're given one of the most controversial "features" of Linux: choice. Those of
us who are mored "advanced" users of Linux know that this choice allows you to
make your operating system truly *yours* - you can customize the looks, tailor
the experience to your workflow, and do some fantastic things that macOS and
Windows users can only dream of. However, what we fail to realize is how the
newcomer feels about the same choice. (There's nothing wrong with this, by the
way, we were all new at some point). The newcomer doesn't want choice. They are
coming from a clear-cut and locked down operating system. This amount of choice You are
able to of "desktop environment" on the front page: think about someone ## 0.
Flashing the ISO This is more of a foreword. Flashing ISOs isn't too difficult,
but the process can definitely be streamlined a little bit. First of all, there
should be more ex ## ISO Let's assume that you have a USB stick with the Manjaro
ISO flashed onto it
