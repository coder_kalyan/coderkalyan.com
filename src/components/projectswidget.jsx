import React from 'react';

export default function ProjectsWidget(props) {
  return (
    <div {...props}>
      <h3 className="text-2xl text-gray-800 lg:text-4xl">
        Projects
      </h3>
      <br />
      <a href="#" className="text-center text-gray-800 hover:text-gray-500 transition-all duration-200">
        <h5 className="text-xl underline lg:text-2xl">QuantumOS</h5>
        <p>
          A simpler, more usable Linux.
        </p>
      </a>
      <br />
      <a href="#" className="text-center text-gray-800 hover:text-gray-500 transition-all duration-200">
        <h5 className="text-xl underline lg:text-2xl">QuantumOS</h5>
        <p>
          A simpler, more usable Linux.
        </p>
      </a>
      <br />
      <a href="#" className="text-center text-gray-800 hover:text-gray-500 transition-all duration-200">
        <h5 className="text-xl underline lg:text-2xl">QuantumOS</h5>
        <p>
          A simpler, more usable Linux.
        </p>
      </a>
    </div>
  );
}
