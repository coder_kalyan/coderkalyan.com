import React from 'react';

export default function ContribWidget(props) {
  return (
    <div {...props}>
      <h3 className="text-2xl text-gray-800 lg:text-4xl">
        Some Projects I have Contributed To
      </h3>
      <br />
      <a href="https://github.com/mavlink/MAVSDK" className="text-center text-gray-800 hover:text-gray-500 transition-all duration-200">
        <h5 className="text-xl underline lg:text-2xl">MAVSDK</h5>
        <p>
            Easy to use MAVLink wrapper library for controlling drones.
        </p>
      </a>
      <br />
      <a href="https://github.com/PX4/Firmware" className="text-center text-gray-800 hover:text-gray-500 transition-all duration-200">
        <h5 className="text-xl underline lg:text-2xl">PX4</h5>
        <p>
            Open source, powerful flight controller firmware for drones and other vehicles.
        </p>
      </a>
      <br />
      <a href="https://github.com/swaywm/wlroots" className="text-center text-gray-800 hover:text-gray-500 transition-all duration-200">
        <h5 className="text-xl underline lg:text-2xl">Wlroots</h5>
        <p>
          Unopinionated, pluggable modules for writing a Wayland compositor.
        </p>
      </a>
    </div>
  );
}
