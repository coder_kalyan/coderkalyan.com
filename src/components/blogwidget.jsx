import React from 'react';
import { Link, StaticQuery, graphql } from 'gatsby';

/* eslint react/prop-types:0 */
function BlogWidget(props) {
  return (
    <div {...props}>
      <StaticQuery
        query={graphql`
          query {
            allMarkdownRemark(
              sort: { order: DESC, fields: [frontmatter___date] }
              limit: 3
              filter: { frontmatter: { draft: {eq: false} } }
            ) {
              nodes {
                html
                frontmatter {
                  draft
                  slug
                  title
                  date
                }
              }
            }
          }
        `}
        render={data => {
          const posts = data.allMarkdownRemark.nodes;
          return (
            <>
              <h3 className="text-2xl text-gray-800 lg:text-4xl">
                Latest Blog Updates
              </h3>
              <br />
              {posts.map((post) => {

                let previewText = post.html.replace(/(<([^>]+)>)/ig, '');
                previewText = previewText.slice(0, 300) + "...";

                return (
                  <div key={post.frontmatter.slug} className="text-center text-gray-800 transition-all duration-200">
                    <Link to={post.frontmatter.slug}>
                      <h5 className="text-xl underline lg:text-2xl">{post.frontmatter.title}</h5>
                    </Link>
                    <p>{previewText}</p>
                    <br />
                  </div>
                );
              })}
              <br />
              <Link to="/blog">
                <p className="underline lg:text-2xl">See All</p>
              </Link>
            </>
          );
        }}
      />
    </div>
  );
}

export default BlogWidget;
