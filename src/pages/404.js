import React from "react";
import SEO from "../components/seo";
import { Link } from "gatsby";

function NotFoundPage() {
  return (
    <div className="flex flex-col w-full h-full font-mono bg-white">
      <SEO title="404: Not found" />
      <main className="w-full px-4 mx-auto" style={{minHeight: "100vh"}}>
        <div className="flex flex-col">
          <div className="text-center yeet-upwards">
            <h1 className="mb-4 text-4xl text-gray-800 title-in typewriter lg:mt-auto lg:text-6xl" style={{marginTop: "20vh"}}>404</h1>
            <p className="mb-2 text-lg text-gray-800 title-in fade-in">The page you requested could not be found.</p>
            <p className="text-gray-500 fade-in">
              <Link to="/" className="underline hover:text-gray-800 transition-all duration-200">Back to Safety</Link>
            </p>
          </div>
        </div>
      </main>
    </div>
  );
}

export default NotFoundPage;
