import React, {useEffect, useState} from 'react';
import { Link } from 'gatsby';
import SEO from '../components/seo';

import ContribWidget from '../components/contribwidget.jsx';
import ProjectsWidget from '../components/projectswidget.jsx';
import BlogWidget from '../components/blogwidget.jsx';

import '../css/animations.css';


function IndexPage() {
  const [ keyPressed, setKeyPressed ] = useState("");
  const [ selectedWidget, setSelectedWidget ] = useState(0);

  const widgets = [
    <ContribWidget key={0} />,
    <ProjectsWidget key={1} />,
    <BlogWidget key={2} />
  ];

  const handleKeyDown = (e) => {
    if (e.key === "h") {
      setKeyPressed(e.key);
    } else if (e.key === "l") {
      setKeyPressed(e.key);
    }
  }

  const handleKeyUp = (e) => {
    if (e.key === "h") {
      setKeyPressed("");
      setSelectedWidget(selectedWidget > 0 ? selectedWidget-1 : widgets.length - 1);
    } else if (e.key === "l") {
      setKeyPressed("");
      setSelectedWidget(selectedWidget < widgets.length-1 ? selectedWidget+1 : 0);
    }
  }

  useEffect(() =>{
    document.addEventListener("keydown", handleKeyDown);
    document.addEventListener("keyup", handleKeyUp);
    return () => {
      document.removeEventListener("keydown", handleKeyDown);
      document.removeEventListener("keyup", handleKeyUp);
    }
  });

  return (
    <div className="flex flex-col w-full h-full font-mono bg-white">
      <SEO
        keywords={[`coder_kalyan`, `home`]}
        title="Home"
      />

      <main className="w-full px-4 mx-auto" style={{minHeight: "100vh"}}>
        <div className="flex flex-col">
          <div className="text-center yeet-upwards">
            <h1 className="mb-4 text-4xl text-gray-800 title-in typewriter lg:mt-auto lg:text-6xl" style={{marginTop: "20vh"}}>~/coder_kalyan</h1>
            <p className="mb-2 text-lg text-gray-800 title-in fade-in">President at AmadorUAVs · Free & Open Source Software Enthusiast · Tech Enthusiast · Linux Geek</p>
            <p className="text-gray-500 fade-in">
              <a className="underline hover:text-gray-800 transition-all duration-200" href="https://gitlab.com/coder_kalyan">Gitlab</a> &middot;&nbsp;
              <a className="underline hover:text-gray-800 transition-all duration-200" href="https://sr.ht/~coder_kalyan">Sourcehut</a> &middot;&nbsp;
              <a className="underline hover:text-gray-800 transition-all duration-200" href="https://www.linkedin.com/in/coderkalyan/">Linkedin</a> &middot;&nbsp;
              <Link to="/blog" className="underline hover:text-gray-800 transition-all duration-200">Blog</Link> &middot;&nbsp;
              <a className="underline hover:text-gray-800 transition-all duration-200" href="https://amadoruavs.com">AmadorUAVs</a>
            </p>
          </div>
        </div>
        <br />
        <br />
        <div className="flex flex-row justify-center w-full mx-auto lg:w-1/2 fade-in2">
          <div className="flex-col hidden mt-32 lg:flex">
            <button className={"hover:bg-gray-400 px-4 py-2 font-bold text-gray-800 rounded-lg transition-all duration-200 " + (keyPressed === "h" ? "bg-gray-500" : "bg-gray-300")} onClick={() => handleKeyUp({key: "h"})}>h</button>
          </div>
          <div className="w-4/5 mx-auto mt-12 text-center">
            {[
              <>{widgets[selectedWidget]}<br/><br/><br/></>,
              ...widgets.map((widget, i) => {
                if (i !== selectedWidget) {
                  return (
                    <>
                      {React.cloneElement(widget, { className: "lg:hidden"})}
                      <br className="lg:hidden"/>
                      <br className="lg:hidden"/>
                      <br className="lg:hidden"/>
                    </>
                  );
                }
              })
            ]}
          </div>
          <div className="flex flex-col hidden mt-32 lg:flex">
            <button className={"hover:bg-gray-400 px-4 py-2 font-bold text-gray-800 rounded-lg transition-all duration-200 " + (keyPressed === "l" ? "bg-gray-500" : "bg-gray-300")} onClick={() => handleKeyUp({key: "l"})}>l</button>
          </div>
        </div>
        <br />
        <br />
      </main>
    </div>
  );
}

export default IndexPage;
