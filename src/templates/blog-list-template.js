import React from 'react';
import { Link, graphql } from 'gatsby';

import SEO from '../components/seo';

import '../css/animations.css';

/* eslint react/prop-types: 0 */
export default function BlogPage({data, pageContext}) {
  const posts = data.allMarkdownRemark.nodes;
  console.log(pageContext.currentPage > 2);

  return (
    <div className="flex flex-col w-full h-full font-mono bg-white">
      <SEO
        keywords={[`coder_kalyan`, `blog`]}
        title="./blog"
      />

      <main className="w-full px-4 mx-auto fade-now" style={{minHeight: "100vh"}}>
        <div className="flex flex-col">
          <div className="text-center">
            <h1 className="block mb-4 text-4xl text-gray-800 title-in lg:mt-auto lg:text-6xl" style={{marginTop: "20vh"}}>~/blog</h1>
            <p className="block mb-2 text-lg text-gray-800 title-in">Blogs.</p>
            <Link to="/" className="mb-2 text-lg text-gray-500 hover:text-gray-800 transition-all duration-200 title-in ">&#x2190; Back to Home</Link>
          </div>
        </div>
        <br />
        <br />
        <div className="flex flex-col justify-center w-full mx-auto lg:w-1/3">
          <div className="flex">
              <Link
                to={"/blog/" + (pageContext.currentPage > 2 ? pageContext.currentPage - 1 : "")}
                className={"mb-2 mr-auto text-lg text-gray-500 hover:text-gray-800 transition-all duration-200 title-in " + (pageContext.currentPage === 1 ? "hidden" : "")}
              >
                &#x2190; Page {pageContext.currentPage - 1}
              </Link>
              <Link
                to={"/blog/" + (pageContext.currentPage + 1)}
                className={"mb-2 text-lg text-gray-500 ml-auto hover:text-gray-800 transition-all duration-200 title-in " + (pageContext.currentPage === pageContext.numPages ? "hidden" : "")}
              >
                &#x2192; Page {pageContext.currentPage + 1}
              </Link>
          </div>
          <br />
          {posts.map((post) => {
            let previewText = post.html.replace(/(<([^>]+)>)/ig, '');
            previewText = previewText.slice(0, 300) + "...";

            return (
              <div
                key={post.frontmatter.slug}
                className="text-center text-gray-800 transition-all duration-200"
              >
                <Link to={post.frontmatter.slug}>
                  <h5 className="block text-xl underline lg:text-2xl">{post.frontmatter.title}</h5>
                </Link>
                <div
                  dangerouslySetInnerHTML={{__html: previewText}}
                  className="blog-post"
                />
                <br />
              </div>
            );
          })}
        </div>
      </main>
    </div>
  );
}

export const pageQuery = graphql`
  query pageQuery($skip: Int!, $limit: Int!) {
    allMarkdownRemark(
      sort: { order: DESC, fields: [frontmatter___date] }
      limit: $limit
      skip: $skip
      filter: { frontmatter: { draft: {eq: false} } }
    ) {
      nodes {
        html
        frontmatter {
          slug
          draft
          title
          date
        }
      }
    }
  }
`
