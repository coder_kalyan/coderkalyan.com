import React from "react";
import { Link, graphql } from "gatsby";

import SEO from '../components/seo.js';
import './blog-template.module.css';

/* eslint react/prop-types: 0 */
export default function BlogTemplate({data}) {
  const { markdownRemark } = data; // data.markdownRemark holds your post data
  const { frontmatter, html } = markdownRemark;
  return (
    <div className="flex flex-col w-full h-full font-mono bg-white">
      <SEO
        keywords={frontmatter.keywords}
        title={frontmatter.title}
      />

      <main className="w-full px-4 mx-auto" style={{minHeight: "100vh"}}>
        <div className="flex flex-col justify-center w-full mx-auto lg:w-1/2 fade-now">
          <br />
          <br />
          <br />
          <Link className="text-xl text-gray-500 underline hover:text-gray-800 transition-all duration-200" to="/blog">&#x2190; Back to Blogs</Link>
          <br />
          <h1 className="text-4xl text-gray-800 lg:text-6xl">{frontmatter.title}</h1>
          <h2 className="text-2xl text-gray-500">{frontmatter.date}</h2>
          <br />
          <br />
          <div
            className="font-serif blog-post-content"
            dangerouslySetInnerHTML={{ __html: html }}
          />
        </div>
      </main>
    </div>
  )
}


export const pageQuery = graphql`
  query($slug: String!) {
    markdownRemark(frontmatter: { slug: { eq: $slug } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        slug
        title
      }
    }
  }
`
